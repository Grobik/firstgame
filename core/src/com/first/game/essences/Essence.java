package com.first.game.essences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.first.game.interfaces.Fights;
import com.first.game.interfaces.Movements;

abstract class Essence implements Movements, Fights {
    protected TextureAtlas texture;

    protected Vector2 currentPoint;
    protected float speed;
    protected float speedBoost;
    protected Vector2 attackArea;
    protected float damage;
    protected float damageBoost;
    protected float heatPoint;
    protected float energyPoint;
    protected float armorPoint;



    //Анимирование
    protected Array<TextureAtlas.AtlasRegion> textureRegions = new Array<TextureAtlas.AtlasRegion>();
    public String previousAnimation;
    public String currentAnimation;
    Animation<TextureRegion> walkAnimation;
    float stateTime = 0f;

    // Характеристики
    public void setHeatPoint(float value) {
        this.heatPoint = value;
    }

    public void setEnergyPoint(float value) {
        this.energyPoint = value;
    }

    public void setArmorPoint(float value) {
        this.armorPoint = value;
    }

    public float getHeatPoint() {
        return this.heatPoint;
    }

    public float getEnergyPoint() {
        return this.energyPoint;
    }

    public float getArmorPoint() {
        return this.armorPoint;
    }

    public void setSpeed(float value) {
        this.speed = value;
    }

    public float getSpeed() {
        return this.speed;
    }

    public void setSpeedBoost(float speedBoost) {
        this.speedBoost = speedBoost;
    }

    public float getSpeedBoost() {
        return speedBoost;
    }

    public void setAttackArea(Vector2 attackArea) {
        this.attackArea = attackArea;
    }

    public Vector2 getAttackArea() {
        return attackArea;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamageBoost(float damageBoost) {
        this.damageBoost = damageBoost;
    }

    public float getDamageBoost() {
        return damageBoost;
    }

    public void setCurrentPoint(Vector2 currentPoint) {
        this.currentPoint = currentPoint;
    }

    public Vector2 getCurrentPoint() {
        return this.currentPoint;
    }

    // Действия

    // - Перемещение
    public void move(int x, int y, boolean status) {

        if (status) {
            if (this.getCurrentPoint().y > y) {
                animation("step_up", 0.1f);
                this.currentPoint = this.currentPoint.sub(new Vector2(1,1));
            } else if (this.getCurrentPoint().y < y) {
                animation("step_down", 0.1f);
                this.currentPoint = this.currentPoint.sub(new Vector2(1,1));
            }
        } else {
            if (this.getCurrentPoint().y > y) {
                animation("static_up", 0.25f);
            } else if (this.getCurrentPoint().y < y) {
                animation("static_down", 0.25f);
            }
        }
    }

    public void jump(int x, int y) {
        this.currentPoint = this.currentPoint.sub(new Vector2(x,y).scl(speed));
    }

    // - Боевая система
    public boolean takeDamage(float damage) {
        this.heatPoint = this.heatPoint - damage;
        return (this.heatPoint - damage) > 0;
    }

    public void swordAttack(Character character) {
        animation("sword_attack_up", 0.05f);
        Vector2 v = character.getCurrentPoint().cpy().sub(this.currentPoint);
        if (character.getHeatPoint() > 0 && this.attackArea.cpy().dot(v) > 0 &&
                this.currentPoint.cpy().sub(character.getCurrentPoint()).len() <= this.attackArea.len())
            character.takeDamage(this.damage);
    }

    public void magicAttack() {
        animation("magic_attack_up", 0.05f);
    }

    public void archerAttack() {
        animation("bow_attack_up", 0.05f);
    }

    public void regionAttack(Character[] characters) {
        for (Character character : characters) {
            Vector2 v = character.getCurrentPoint().cpy().sub(this.currentPoint);
            if (character.getHeatPoint() > 0 && this.attackArea.cpy().dot(v) > 0 &&
                    this.currentPoint.cpy().sub(character.getCurrentPoint()).len() <= this.attackArea.len())
                character.takeDamage(this.damage);
        }
    }

    //Анимирование
    public void animation(String value, float frameDuration) {
        try {
            System.out.println(value);
            this.currentAnimation = value;
            textureRegions.clear();
            textureRegions.addAll(texture.findRegions(value));
            walkAnimation = new Animation<TextureRegion>(frameDuration, textureRegions);
            stateTime = 0f;
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void draw(SpriteBatch batch) {
        if (stateTime >= 30f)
            animation("game", 0.25f);

        stateTime += Gdx.graphics.getDeltaTime();
        TextureRegion currentFrame = walkAnimation.getKeyFrame(stateTime, true);
        batch.draw(currentFrame, this.currentPoint.x, this.currentPoint.y);
    }
}
