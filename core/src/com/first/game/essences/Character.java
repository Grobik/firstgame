package com.first.game.essences;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Character extends Essence {

    public Character(TextureAtlas texture, Vector2 currentPoint) {
        this.texture = texture;
        this.heatPoint = 100;
        this.energyPoint = 100;
        this.armorPoint = 100;
        this.currentPoint = currentPoint;
        this.speed = 0.01f;
        this.speedBoost = 1;
        this.attackArea = new Vector2(15,15);
        this.damage = 1;
        this.damageBoost = 1;
    }

    public Character(TextureAtlas texture, float heatPoint, float energyPoint, float armorPoint,
                     Vector2 currentPoint, float speed, float speedBoost,
                     float damage, float damageBoost) {
        this.texture = texture;
        this.heatPoint = heatPoint;
        this.energyPoint = energyPoint;
        this.armorPoint = armorPoint;
        this.currentPoint = currentPoint;
        this.speed = speed;
        this.speedBoost = speedBoost;
        this.damage = damage;
        this.damageBoost = damageBoost;
    }
}
