package com.first.game;

import com.badlogic.gdx.Game;
import com.first.game.screens.menu.MenuScreen;

public class NewGame extends Game {
	@Override
	public void create() {
		setScreen(new MenuScreen(this));
	}
}
