package com.first.game.locations;

import com.badlogic.gdx.graphics.Texture;

public class GameLocation {
    //Параметры локации
    private int width;
    private int height;
    private String name;
    private Texture background = new Texture("background.jpg");

    //Параметры врагов
    protected int amountEnemy;

    public GameLocation(String name, int width, int height, Texture background) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.background = background;
    }

    public Texture getBackground() {
        return background;
    }
}
