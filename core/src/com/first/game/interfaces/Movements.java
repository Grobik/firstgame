package com.first.game.interfaces;

public interface Movements {
    void move(int x, int y, boolean status);
    void jump(int x, int y);
}
