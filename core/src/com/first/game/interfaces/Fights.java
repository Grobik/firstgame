package com.first.game.interfaces;

import com.first.game.essences.Character;

public interface Fights {
    boolean takeDamage(float damage);
    void swordAttack(Character character);
    void magicAttack();
    void archerAttack();
    void regionAttack(Character[] characters);
}
