package com.first.game.screens.menu;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.first.game.screens.menu.buttons.Exit;
import com.first.game.screens.menu.buttons.NewGame;
import com.first.game.screens.process.ProcessScreen;

import engine.screen.BaseScreen;
import engine.ui.ActionListener;

public class MenuScreen extends BaseScreen implements ActionListener {

    private NewGame buttonNewGame;
    private Exit buttonExit;
    private TextureAtlas buttonAtlas;


    private TextureRegion region;
    private Sprite sprite;

    public MenuScreen(Game game) {
        super(game);
        buttonAtlas = new TextureAtlas("buttons-atlas.atlas");
        buttonNewGame = new NewGame(buttonAtlas,this,1f);
        buttonExit = new Exit(buttonAtlas,this,1f);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void show () {
        super.show();
        batch = new SpriteBatch();
    }

    @Override
    public void render (float delta) {
        super.render(delta);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        buttonNewGame.draw(batch);
        buttonExit.draw(batch);
        batch.end();
    }

    @Override
    public void dispose () {
        batch.dispose();
        buttonAtlas.dispose();
        super.dispose();
    }

    @Override
    public boolean keyUp (int keycode) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        buttonNewGame.touchDown(new Vector2(x,y), pointer);
        buttonExit.touchDown(new Vector2(x,y), pointer);
        return true;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        buttonNewGame.touchUp(new Vector2(x,y), pointer);
        buttonExit.touchUp(new Vector2(x,y), pointer);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void actionPerformed(Object src) {
        if (src == buttonExit)
            Gdx.app.exit();
        else if (src == buttonNewGame)
            game.setScreen(new ProcessScreen(game));
        else
            throw new RuntimeException("error - " + src);
    }
}
