package com.first.game.screens.process.buttons;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import engine.ui.ActionListener;

public class SwordAttack {

    private int pointer;
    private TextureRegion region;
    private boolean pressed;
    private final ActionListener listener;
    private float pressScale;
    private Vector2 position;

    public SwordAttack(TextureAtlas textureAtlas, ActionListener listener, float pressScale) {
        this.region = textureAtlas.findRegion("sword");
        this.listener = listener;
        this.pressScale = pressScale;
        this.position = new Vector2(250,50);
    }

    public boolean touchDown(Vector2 touch, int pointer) {
        if (pressed)
            return false;
        this.pointer = pointer;
        return pressed = true;
    }

    public boolean touchUp(Vector2 touch, int pointer) {
        if (!pressed)
            return false;
        if (clickButton(touch))
            listener.actionPerformed(this);

        this.pointer = pointer;
        return pressed = false;
    }

    private boolean clickButton(Vector2 touch) {
        return touch.x >= position.x && touch.x <= region.getRegionWidth() + position.x && touch.y >= position.y && touch.y <= region.getRegionHeight() + position.y;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(region, position.x, position.y);
    }
}
