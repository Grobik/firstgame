package com.first.game.screens.process;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.first.game.essences.Character;
import com.first.game.screens.process.buttons.BowAttack;
import com.first.game.screens.process.buttons.MagicAttack;
import com.first.game.screens.process.buttons.SwordAttack;

import engine.screen.BaseScreen;
import engine.ui.ActionListener;

public class ProcessScreen extends BaseScreen implements ActionListener {
    //Переменные
    private boolean activeTouch = false;
    private int width = Gdx.graphics.getWidth();
    private int height = Gdx.graphics.getHeight();

    //Текстуры
    private Texture locationTexture;
    private TextureAtlas playerAtlas;
    private TextureAtlas buttonAtlas;

    //Объекты
    private Character player;
    private SwordAttack buttonSwordAttack;
    private BowAttack buttonBowAttack;
    private MagicAttack buttonMagicAttack;

    public ProcessScreen(Game game) {
        super(game);
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
        super.resize(width, height);
    }

    @Override
    public void show () {
        super.show();
        batch = new SpriteBatch();

        locationTexture = new Texture("background.jpg");
        playerAtlas = new TextureAtlas("green-elf-atlas.atlas");
        player = new Character(playerAtlas, new Vector2(this.width / 2,this.height / 2));
        player.animation("static_down", 0.25f);

        buttonAtlas = new TextureAtlas("buttons-atlas.atlas");
        buttonSwordAttack = new SwordAttack(buttonAtlas,this,1f);
        buttonBowAttack = new BowAttack(buttonAtlas,this,1f);
        buttonMagicAttack = new MagicAttack(buttonAtlas,this,1f);
    }

    @Override
    public void render (float delta) {
        super.render(delta);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        player.draw(batch);
        buttonSwordAttack.draw(batch);
        buttonBowAttack.draw(batch);
        buttonMagicAttack.draw(batch);
        batch.end();
    }

    @Override
    public void dispose () {
        batch.dispose();
        locationTexture.dispose();
        playerAtlas.dispose();
        buttonAtlas.dispose();
        super.dispose();
    }

    @Override
    public boolean keyUp (int keycode) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        buttonSwordAttack.touchUp(new Vector2(x,y), pointer);
        buttonBowAttack.touchUp(new Vector2(x,y), pointer);
        buttonMagicAttack.touchUp(new Vector2(x,y), pointer);
        player.move(x, y, true);
        return false;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        buttonSwordAttack.touchUp(new Vector2(x,y), pointer);
        buttonBowAttack.touchUp(new Vector2(x,y), pointer);
        buttonMagicAttack.touchUp(new Vector2(x,y), pointer);
        player.move(x, y, false);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void actionPerformed(Object src) {
        System.out.println(src);
        if (src == buttonSwordAttack)
            player.swordAttack(player);
        else if (src == buttonBowAttack)
            player.archerAttack();
        else if (src == buttonMagicAttack)
            player.magicAttack();
        else
            throw new RuntimeException("error - " + src);
    }
}
