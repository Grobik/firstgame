package engine.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class BaseScreen implements Screen, InputProcessor {

    protected Game game;
    protected SpriteBatch batch;
    protected int width;
    protected int height;

    public BaseScreen(Game game) {
        this.game = game;
        Gdx.input.setInputProcessor(this);
    }

    //Screen

    @Override
    public void show () {
        if (this.batch != null) {
            throw new RuntimeException("Batch != null");
        }
    }

    @Override
    public void render (float delta) {
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void pause() {
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose () {
    }

    //InputProcessor

    @Override
    public boolean keyUp (int keycode) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        //System.out.println("keyDown " + keycode);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        //System.out.println("keyTyped character = " + character);
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        //System.out.println("\ntouchDown Vector2( " + screenX + ", " + screenY + " )");
        //System.out.println("touchDown pointer = " + pointer + ", button = " + button);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        //System.out.println("touchUp screenX = " + screenX + ", screenY = " + screenY);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        //System.out.println("touchDragged screenX = " + screenX + ", screenY = " + screenY + "----" + pointer);
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        //System.out.println("touchUp screenX = " + screenX + ", screenY = " + screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        //System.out.println(amount);
        return false;
    }
}
