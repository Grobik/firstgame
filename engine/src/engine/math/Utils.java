package engine.math;


import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;

public class Utils {

    private Utils() {
    }

    public static void method() {
        Vector2 v1 = new Vector2(1,3);
        Vector2 v2 = new Vector2(0, -1);
        Vector2 v3 = v1.cpy().add(v2);
        System.out.println("v1.x = " + v1.x + ", v1.y = " + v1.y);
        System.out.println("v3.x = " + v3.x + ", v3.y = " + v3.y);

        Vector2 v4 = new Vector2(1,1);
        Vector2 v5 = new Vector2(4,3);
        v5.sub(v4);
        System.out.println("v5.x = " + v5.x + ", v5.y = " + v5.y);

        Vector2 v6 = new Vector2(10, 20);
        v6.scl(0.9f);
        System.out.println("v6.x = " + v6.x + ", v6.y = " + v6.y);

        Vector2 v7 = new Vector2(3,2);
        Vector2 v8 = new Vector2(1,1);
        v7.sub(v8);
        System.out.println("v8.len = " + v8.len());

        Vector2 v9 = new Vector2(1, 1);
        Vector2 v10 = new Vector2(-1, 1);
        v9.nor();
        v10.nor();
        System.out.println(Math.acos(v9.dot(v10)));

        Matrix3 m1 = new Matrix3();
        m1.idt();
        v6.mul(m1);
        System.out.println("v6.x = " + v6.x + ", v6.y = " + v6.y);

    }
}
