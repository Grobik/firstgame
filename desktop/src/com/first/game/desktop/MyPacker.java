package com.first.game.desktop;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class MyPacker {
    public static void main (String[] args) throws Exception {
        TexturePacker.process("buttons-atlas-src", "android/assets", "buttons-atlas");
    }
}
