package com.first.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.first.game.NewGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name","Public");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		//float aspect = 480f/854f;
		float aspect = 3f/4f;
		config.height = 500;
		config.width = (int) (config.height * aspect);
		new LwjglApplication(new NewGame(), config);
	}
}
